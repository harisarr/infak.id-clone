import { Button, Typography, Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import CancelOutlinedIcon from "@material-ui/icons/CancelOutlined";
import Image from "next/image";

function NavbarSecond() {
  // const [isScrolled, setScrolled] = useState(false);
  const [selectedMenu, setSelectedMenu] = useState("beranda");
  const [isMobile, setisMobile] = useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const router = useRouter();

  // const handleScroll = () => {
  //   console.log(window.scrollBy);
  //   if (window.scrollY > 0) {
  //     setScrolled(true);
  //   } else {
  //     setScrolled(false);
  //   }
  // };
  // useEffect(() => {
  //   window.addEventListener("scroll", handleScroll);
  //   return () => window.removeEventListener("scroll", handleScroll);
  // });
  useEffect(() => {
    if (isMob) {
      setisMobile(isMob);
    } else {
      setisMobile(false);
    }
  }, [isMob]);
  useEffect(() => {
    console.log(router.pathname);

    if (router.pathname === "/TentangKami") {
      setSelectedMenu("tentang-kami");
    } else if (router.pathname === "/berita-dan-program") {
      setSelectedMenu("berita-dan-program");
    } else if (router.pathname && "/DonaturTerikini") {
      setSelectedMenu("donatur-terkini");
    }
  }, [router.pathname]);

  // console.log(isScrolled);

  const [state, setState] = React.useState({
    right: false,
  });
  const data = [
    {
      push: "/DonaturTerkini",
      name: "Donatur Terkini",
    },
    {
      push: "/berita-dan-program",
      name: "Berita dan Program",
    },
    {
      push: "/TentangKami",
      name: "Tentang Kami",
    },
  ];

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List
        style={{
          width: 192,
          textAlign: "end",
          padding: 0,
          margin: "16px 19px",
        }}
      >
        <CancelOutlinedIcon style={{ color: "#2DBE78" }} />
        <Typography style={{ fontSize: 14 }}>
          {data.map((item) => (
            <p onClick={() => router.push(`${item.push}`)}>{item.name}</p>
          ))}
        </Typography>
        <Button
          style={{
            fontWeight: 600,
            fontSize: 16,
            marginLeft: 3,
            cursor: "pointer",
            whiteSpace: "nowrap",
            border: "1px solid #4D4D4D",
            padding: "6px 15px",
            borderRadius: 4,
            textTransform: "inherit",
          }}
        >
          Donasi Sekarang
        </Button>
      </List>
    </div>
  );

  return (
    <>
      {isMobile ? (
        <>
          <Grid
            style={{
              display: "flex",
              padding: 15,
              alignItems: "center",
              position: "fixed",
              background: "#FFFFFF",
              width: "100%",
              boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
              zIndex: 999,
            }}
          >
            <Grid
              item
              xs={6}
              style={{
                textAlign: "left",
                alignSelf: "center",
                display: "flex",
              }}
            >
              <Image
                onClick={() => router.push("/")}
                src="/Frame/InfakOnline-Green.svg"
                width={129}
                height={22.38}
              />
            </Grid>
            <Grid
              item
              xs={6}
              style={{ textAlign: "right", alignSelf: "center" }}
            >
              {["right"].map((anchor) => (
                <Grid key={anchor}>
                  <Button onClick={toggleDrawer(anchor, true)}>
                    <Grid style={{ justifyContent: "right" }}>
                      <Image src="/Icon/responsive-menu.svg" layout="fill" />
                    </Grid>
                  </Button>
                  <Drawer
                    anchor={anchor}
                    open={state[anchor]}
                    onClose={toggleDrawer(anchor, false)}
                  >
                    {list(anchor)}
                  </Drawer>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </>
      ) : (
        <div
          style={{
            padding: "21px 106px",
            display: "flex",
            width: "100%",
            height: 72,
            position: "fixed",
            zIndex: 999,
            background: "#FFFFFF",
            boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
            // boxShadow: isScrolled ? "0px 2px 4px rgba(0, 0, 0, 0.25)" : null,
            top: 0,
          }}
        >
          <Grid
            item
            xs={3}
            style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              cursor: "pointer",
            }}
          >
            <Image
              onClick={() => router.push("/")}
              src="/Frame/InfakOnline-Green.svg"
              width={169}
              height={23.5}
            />
          </Grid>
          <Grid
            item
            xs={9}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
            }}
          >
            <div style={{ display: "flex", flexDirection: "column" }}>
              <Typography
                onClick={() => router.push("/DonaturTerkini")}
                style={{
                  fontWeight: 600,
                  fontSize: 16,
                  color: "#4D4D4D",
                  margin: "0px 3px",
                  cursor: "pointer",
                  whiteSpace: "nowrap",
                  padding: "0px 13px",
                  margin: "0px 3px",
                }}
              >
                Donatur Terkini
              </Typography>
              {selectedMenu === "donatur-terkini" && (
                <div style={{ alignSelf: "center" }}>
                  <div
                    style={{
                      width: 98,
                      marginTop: 3,
                      marginBottom: -7,
                      border: "2px solid #4D4D4D",
                      borderRadius: 3,
                    }}
                  />
                </div>
              )}
            </div>

            <div style={{ display: "flex", flexDirection: "column" }}>
              <Typography
                onClick={() => router.push("/berita-dan-program")}
                style={{
                  fontWeight: 600,
                  fontSize: 16,
                  color: "#4D4D4D",
                  margin: "0px 3px",
                  cursor: "pointer",
                  whiteSpace: "nowrap",
                  padding: "0px 13px",
                }}
              >
                Berita dan Program
              </Typography>
              {selectedMenu === "berita-dan-program" && (
                <div style={{ alignSelf: "center" }}>
                  <div
                    style={{
                      width: 98,
                      marginTop: 3,
                      marginBottom: -7,
                      border: "2px solid #4D4D4D",
                      borderRadius: 3,
                    }}
                  />
                </div>
              )}
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "column",
                margin: "0px 16px",
              }}
            >
              <Typography
                onClick={() => router.push("/TentangKami")}
                style={{
                  fontWeight: 600,
                  fontSize: 16,
                  color: "#4D4D4D",
                  cursor: "pointer",
                  whiteSpace: "nowrap",
                  padding: "0px ",
                }}
              >
                Tentang Kami
              </Typography>
              {selectedMenu === "tentang-kami" && (
                <div style={{ alignSelf: "center" }}>
                  <div
                    style={{
                      width: 98,
                      marginTop: 3,
                      marginBottom: -7,
                      border: "2px solid #4D4D4D",
                      borderRadius: 3,
                    }}
                  />
                </div>
              )}
            </div>

            <Button
              style={{
                fontWeight: 600,
                fontSize: 16,
                color: "#4D4D4D",
                marginLeft: 3,
                cursor: "pointer",
                whiteSpace: "nowrap",
                border: "1px solid #4D4D4D",
                padding: "6px 15px",
                borderRadius: 4,
                textTransform: "inherit",
                display: "flex",
                alignItems: "none",
                justifyContent: "none",
              }}
            >
              Donasi Sekarang
            </Button>
          </Grid>
        </div>
      )}
    </>
  );
}

export default NavbarSecond;
