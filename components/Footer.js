import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import Image from "next/image";
import { useRouter } from "next/router";

function Footer() {
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const router = useRouter();
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  const data = [
    { icon: "/Icon/Med-1.svg" },
    { icon: "/Icon/Med-2.svg" },
    { icon: "/Icon/Med-3.svg" },
    { icon: "/Icon/Med-4.svg" },
  ];
  const data2 = [
    { name: "Donatur Terkini", push: "/DonaturTerkini" },
    { name: "Berita dan Program", push: "/berita-dan-program" },
    { name: "Tentang Kami", push: "/TentangKami" },
  ];

  return (
    <Grid>
      <>
        {isMobile ? (
          <Grid
            style={{
              backgroundImage: "url(/Frame/HomepageMainFrame.png)",
              backgroundSize: "cover",
              width: "100%",
              padding: "33px 21px",
            }}
          >
            <Grid>
              <Grid style={{ justifyContent: "flex-start" }}>
                <Grid style={{ display: "flex", marginBottom: 9 }}>
                  <Image src="/Frame/Logo2.svg" width={52} height={52} />
                  <div style={{ marginLeft: 16 }}>
                    <Image
                      width={137}
                      height={50}
                      src="/Frame/InfakOnline-White.svg"
                    />
                  </div>
                </Grid>
                <Typography
                  style={{
                    fontWeight: 600,
                    fontSize: 14,
                    color: "#FAFAFA",
                    marginBottom: 26,
                  }}
                >
                  infakonline adalah platform donasi online untuk memudahkan
                  donatur dalam menitipkan dana infak / sedekah dengan beragam
                  pilihan pembayaran digital.
                </Typography>
              </Grid>
              <Grid>
                <Typography
                  style={{
                    fontSize: 15,
                    fontWeight: 600,
                    color: "#FAFAFA",
                    marginBottom: 20,
                    width: "70%",
                  }}
                >
                  Yayasan Pondok Pesantren Imam Syafi'i (Pontrenis)
                </Typography>

                <Typography
                  style={{ fontSize: 13, fontWeight: 400, color: "#FAFAFA" }}
                >
                  Telepon: (0778) 3851843
                </Typography>

                <Typography
                  style={{
                    fontSize: 13,
                    fontWeight: 400,
                    color: "#FAFAFA",
                    width: "83%",
                    marginBottom: 26,
                  }}
                >
                  Alamat: Perum PJB Perumahan Taman Batu Aji Indah Tahap 2,
                  Sagulung, Sungai Binti, Batam, Kota Batam, Kepulauan Riau
                  29439
                </Typography>
              </Grid>
            </Grid>
            <Grid>
              <Grid style={{ marginBottom: 26 }}>
                {data2.map((item) => (
                  <Typography
                    style={{
                      fontSize: 15,
                      fontWeight: 700,
                      color: "#FAFAFA",
                      marginBottom: 19,
                    }}
                  >
                    {item.name}
                  </Typography>
                ))}
              </Grid>
              <Grid style={{ display: "flex" }}>
                {data.map((item) => (
                  <Grid style={{ marginRight: 7 }}>
                    <Image src={item.icon} width={34} height={34} />
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        ) : (
          <Grid
            style={{
              backgroundImage: "url(/Frame/HomepageMainFrame.png)",
              backgroundSize: "cover",
              width: "100%",
              padding: "25px 92px",
              display: "flex",
            }}
          >
            <Grid item xs={6} style={{ display: "flex" }}>
              <Grid style={{ justifyContent: "flex-start" }}>
                <Grid style={{ display: "flex", marginBottom: 9 }}>
                  <Image src="/Frame/Logo2.svg" width={52} height={52} />
                  <div style={{ marginLeft: 16 }}>
                    <Image
                      width={137}
                      height={50}
                      src="/Frame/InfakOnline-White.svg"
                    />
                  </div>
                </Grid>
                <Typography
                  style={{
                    fontWeight: 600,
                    fontSize: 13,
                    color: "#FAFAFA",
                  }}
                >
                  infakonline adalah platform donasi online untuk memudahkan
                  donatur dalam menitipkan dana infak / sedekah dengan beragam
                  pilihan pembayaran digital.
                </Typography>
              </Grid>
              <Grid style={{ paddingLeft: 50 }}>
                <Typography
                  onClick={() => router.push(`${item.push}`)}
                  style={{
                    fontSize: 15,
                    fontWeight: 600,
                    marginBottom: 18,
                    color: "#FAFAFA",
                  }}
                >
                  Yayasan Pondok Pesantren Imam Syafi'i (Pontrenis)
                </Typography>

                <Typography
                  style={{ fontSize: 13, fontWeight: 400, color: "#FAFAFA" }}
                >
                  Telepon: (0778) 3851843
                </Typography>

                <Typography
                  style={{ fontSize: 13, fontWeight: 400, color: "#FAFAFA" }}
                >
                  Alamat: Perum PJB Perumahan Taman Batu Aji Indah Tahap 2,
                  Sagulung, Sungai Binti, Batam, Kota Batam, Kepulauan Riau
                  29439
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs={6} style={{ display: "flex" }}>
              <Grid item xs={6} style={{ marginLeft: 60 }}>
                {data2.map((item) => (
                  <Typography
                    onClick={() => router.push(`${item.push}`)}
                    style={{
                      fontSize: 15,
                      fontWeight: 700,
                      color: "#FAFAFA",
                      marginBottom: 19,
                      cursor: "pointer",
                    }}
                  >
                    {item.name}
                  </Typography>
                ))}
              </Grid>
              <Grid
                item
                xs={6}
                style={{
                  justifyContent: "flex-end",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                {data.map((item) => (
                  <Grid style={{ marginLeft: 7 }}>
                    <Image src={item.icon} width={34} height={34} />
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        )}
      </>
    </Grid>
  );
}

export default Footer;
