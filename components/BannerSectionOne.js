import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import DonationCard from "./donation-card/DonationCard";
import Image from "next/image";
import { useRouter } from "next/router";
import "antd/dist/antd.css";

function BannerSectionOne() {
  const router = useRouter();
  const [isMobile, setisMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setisMobile(isMob);
    } else {
      setisMobile(false);
    }
  }, [isMob]);
  const data = [
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
  ];
  return (
    <>
      {isMob ? (
        <Grid
          style={{
            paddingTop: 26,
            paddingLeft: 7,
            paddingRight: 7,
            width: "100%",
          }}
        >
          <Grid>
            <Typography
              style={{
                fontWeight: 700,
                color: "#FAFAFA",
                fontSize: 20,
                marginBottom: 3,
                whiteSpace: "nowrap",
                padding: "0px 8px",
              }}
            >
              Sedekah membawa berkah.
            </Typography>
            <Typography
              style={{
                fontWeight: 600,
                fontSize: 9,
                color: "#FAFAFA",
                padding: "0px 8px",
              }}
            >
              infakonline adalah platform donasi online untuk memudahkan donatur
              dalam menitipkan dana infak / sedekah dengan beragam pilihan
              pembayaran digital.
            </Typography>
            <Grid
              style={{
                background: "white",
                borderRadius: 8,
                boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
                marginTop: 18,
                marginBottom: 42,
              }}
            >
              <DonationCard />
            </Grid>
          </Grid>
          <Grid style={{ width: "100%" }}>
            <Grid
              style={{
                background: "white",
                borderRadius: 8,
                boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
              }}
            >
              <Grid style={{ padding: "7px 9px" }}>
                <Grid
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Grid item xs={6}>
                    <Typography
                      style={{
                        fontSize: 9,
                        fontWeight: 600,
                        color: "#4d4d4d",
                        whiteSpace: "nowrap",
                      }}
                    >
                      Jumlah Infak terkumpul
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Image src="/Icon/Verified.svg" width={10} height={10} />
                    <Typography
                      style={{
                        fontWeight: 600,
                        fontSize: 9,
                        color: "#2DBE78",
                        marginLeft: 9,
                      }}
                    >
                      345
                    </Typography>
                  </Grid>
                </Grid>
                <Typography
                  style={{
                    fontSize: 28,
                    fontWeight: 700,
                    paddingLeft: 10,
                  }}
                >
                  Rp 201.140.000
                </Typography>
              </Grid>

              <div
                style={{
                  border: "1px solid #F5F5F5",
                  width: "100%",
                }}
              />

              <Grid style={{ padding: "7px 9px" }}>
                <Grid style={{ display: "flex" }}>
                  <Typography
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                      fontWeight: 700,
                      fontSize: 10,
                      color: "#4D4D4D",
                      width: "50%",
                      marginBottom: 4,
                    }}
                  >
                    Donatur Terkini
                  </Typography>
                </Grid>

                <Grid
                  style={{
                    display: "flex",
                    border: "1px solid #2DBE78",
                    borderRadius: 4,
                    marginBottom: 3,
                  }}
                >
                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#2DBE78",
                        color: "#FAFAFA",
                        alignItems: "center",
                        fontSize: 9,
                        fontWeight: 600,
                        padding: "2px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Donatur
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 9,
                        fontWeight: 600,
                        padding: "2px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Via
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 9,
                        fontWeight: 600,
                        padding: "2px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Nominal
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 9,
                        fontWeight: 600,
                        padding: "2px 0px",
                        borderRadius: 2,
                        textAlign: "center",
                        whiteSpace: "nowrap",
                      }}
                    >
                      Waktu Donasi
                    </Typography>
                  </Grid>
                </Grid>

                {data.map((item) => (
                  <Grid style={{ display: "flex", paddingTop: 5 }}>
                    <Grid item xs={4} style={{ alignSelf: "center" }}>
                      <Typography
                        style={{
                          background: "#FFFFF",
                          color: "#4D4D4D",
                          fontSize: 9,
                          fontWeight: 600,
                          textAlign: "start",
                          paddingLeft: 15,
                        }}
                      >
                        {item.donatur}
                      </Typography>
                    </Grid>
                    <Grid item xs={4} style={{ alignSelf: "center" }}>
                      <Typography
                        style={{
                          background: "#FFFFF",
                          color: "#4D4D4D",
                          fontSize: 9,
                          fontWeight: 600,
                          textAlign: "start",
                          paddingLeft: 15,
                        }}
                      >
                        {item.via}
                      </Typography>
                    </Grid>
                    <Grid item xs={4} style={{ alignSelf: "center" }}>
                      <Typography
                        style={{
                          background: "#FFFFF",
                          color: "#4D4D4D",
                          fontSize: 9,
                          fontWeight: 600,
                          textAlign: "start",
                          paddingLeft: 15,
                        }}
                      >
                        {item.nominal}
                      </Typography>
                    </Grid>
                    <Grid item xs={4} style={{ alignSelf: "center" }}>
                      <Typography
                        style={{
                          background: "#FFFFF",
                          color: "#4D4D4D",
                          fontSize: 9,
                          fontWeight: 600,
                          textAlign: "start",
                          paddingLeft: 15,
                        }}
                      >
                        {item.time}
                      </Typography>
                    </Grid>
                  </Grid>
                ))}
              </Grid>

              <div
                style={{
                  border: "1px solid #F5F5F5",
                  width: "100%",
                }}
              />
              <Typography
                onClick={() => router.push("/DonaturTerkini")}
                style={{
                  fontSize: 12,
                  textAlign: "center",
                  alignItems: "center",
                  color: "#28A96B",
                  padding: "10px 0px",
                }}
              >
                Lihat semua
              </Typography>
            </Grid>
          </Grid>
          {/* <Grid style={{ marginRight: 60 }} /> */}
        </Grid>
      ) : (
        <>
          <Grid
            style={{
              paddingTop: 35,
              display: "flex",
              // paddingLeft: 187,
              // paddingRight: 187,
              paddingLeft: 106,
              paddingRight: 106,
            }}
          >
            <Grid item xs={7}>
              <Grid
                style={{
                  background: "white",
                  borderRadius: 8,
                  boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
                }}
              >
                <Grid style={{ padding: "0px 16px", paddingTop: 13 }}>
                  <Grid style={{ display: "flex" }}>
                    <Grid item xs={6}>
                      <Typography
                        style={{
                          fontSize: 14,
                          fontWeight: 600,
                          color: "#4d4d4d",
                          whiteSpace: "nowrap",
                        }}
                      >
                        Jumlah Infak terkumpul
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      style={{ display: "flex", justifyContent: "flex-end" }}
                    >
                      <Image src="/Icon/Verified.svg" width={17} height={24} />
                      <Typography
                        style={{
                          fontWeight: 600,
                          fontSize: 16,
                          color: "#2DBE78",
                          marginLeft: 9,
                        }}
                      >
                        345
                      </Typography>
                    </Grid>
                  </Grid>
                  <Typography
                    style={{
                      fontSize: 40,
                      fontWeight: 700,
                      marginLeft: 17,
                      marginTop: 8,
                    }}
                  >
                    Rp 201.140.000
                  </Typography>
                </Grid>

                <div
                  style={{
                    border: "1px solid #F5F5F5",
                    width: "100%",
                    marginTop: 25,
                    marginBottom: 11,
                  }}
                />

                <Grid style={{ padding: "0px 16px" }}>
                  <Grid style={{ display: "flex" }}>
                    <Typography
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        fontWeight: 700,
                        fontSize: 14,
                        color: "#4D4D4D",
                        width: "50%",
                        marginBottom: 8,
                      }}
                    >
                      Donatur Terkini
                    </Typography>
                  </Grid>

                  <Grid
                    style={{
                      display: "flex",
                      border: "1px solid #2DBE78",
                      borderRadius: 4,
                      marginBottom: 5,
                    }}
                  >
                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#2DBE78",
                          color: "#FAFAFA",
                          alignItems: "center",
                          fontSize: 12,
                          fontWeight: 600,
                          padding: "7px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Donatur
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 12,
                          fontWeight: 600,
                          padding: "7px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Via
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 12,
                          fontWeight: 600,
                          padding: "7px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Nominal
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 12,
                          fontWeight: 600,
                          padding: "7px 0px",
                          borderRadius: 2,
                          textAlign: "center",
                          whiteSpace: "nowrap",
                        }}
                      >
                        Waktu Donasi
                      </Typography>
                    </Grid>
                  </Grid>

                  {data.map((item) => (
                    <Grid style={{ display: "flex", paddingTop: 8 }}>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.donatur}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.via}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.nominal}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.time}
                        </Typography>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>

                <div
                  style={{
                    border: "1px solid #F5F5F5",
                    width: "100%",
                    marginTop: 15,
                  }}
                />
                <Typography
                  onClick={() => router.push("/DonaturTerkini")}
                  style={{
                    fontSize: 14,
                    textAlign: "center",
                    alignItems: "center",
                    color: "#28A96B",
                    padding: 15,
                  }}
                >
                  Lihat semua
                </Typography>
              </Grid>

              <Typography
                style={{
                  fontWeight: 700,
                  color: "#FAFAFA",
                  fontSize: 35,
                  marginTop: 23,
                  marginBottom: 3,
                  whiteSpace: "nowrap",
                }}
              >
                Sedekah membawa berkah.
              </Typography>
              <Typography
                onClick={() => router.push("/DonaturTerkini")}
                style={{
                  fontWeight: 600,
                  fontSize: 16,
                  color: "#FAFAFA",
                  marginRight: 20,
                }}
              >
                infakonline adalah platform donasi online untuk memudahkan
                donatur dalam menitipkan dana infak / sedekah dengan beragam
                pilihan pembayaran digital.
              </Typography>
            </Grid>

            {/* <Grid item xs={1} style={{ marginRight: 60 }} /> */}
            <Grid
              item
              xs={5}
              style={{
                background: "white",
                padding: 28,
                borderRadius: 8,
                boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
                marginLeft: 50,
              }}
            >
              <DonationCard />
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export default BannerSectionOne;
