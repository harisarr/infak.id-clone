import React from "react";
import { Grid, Button, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import ChevronRightRoundedIcon from "@material-ui/icons/ChevronRightRounded";
import KeyboardArrowDownRoundedIcon from "@material-ui/icons/KeyboardArrowDownRounded";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useMediaQuery } from "react-responsive";
import Checkbox from "@material-ui/core/Checkbox";
import PaymentMethod from "./PaymentMethod";
import Radio from "@material-ui/core/Radio";
import { getListProducts } from "../../services/getListProduct";

const GreenCheckbox = withStyles({
  root: {
    color: "#4D4D4D",
    "&$checked": {
      color: "#4D4D4D",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const GreenRadio = withStyles({
  root: {
    color: "#2DBE78",
    "&$checked": {
      color: "#2DBE78",
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

function DonationCard() {
  const [isMobile, setisMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  React.useEffect(() => {
    if (isMob) {
      setisMobile(isMob);
    } else {
      setisMobile(false);
    }
  }, [isMob]);

  const [products, setProducts] = React.useState([]);

  // console.log(products);

  React.useEffect(() => {
    async function getData() {
      const { data } = await getListProducts();
      setProducts(data);
      // setSelectedProductId(data[0].id);
      setSelectedProduct(data[0]);
      console.log(data);
    }
    getData();
  }, []);

  const [selectedPaymentMethod, setSelectedPaymentMethod] = React.useState(
    null
  );
  const [state, setState] = React.useState({
    checkedG: false,
  });
  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const [cardDisplay, setCardDisplay] = React.useState("");

  const [selectedProductId, setSelectedProductId] = React.useState("a");
  const [selectedProduct, setSelectedProduct] = React.useState(null);

  const handleChangeProduct = (event) => {
    // console.log(event);
    setSelectedProductId(event.target.value);
  };
  // console.log(selectedPaymentMethod);
  const [customPrice, setCustomPrice] = React.useState(0);

  const handleChangeCustomPrice = (event) => {
    setCustomPrice(event.target.value);
    setIsValidPrice(true);
  };

  const [isValidPrice, setIsValidPrice] = React.useState(true);

  const handleSubmit = (event) => {
    if (selectedProduct) {
      if (
        Number(customPrice) <
        selectedProduct.customPriceConfig.minimumCustomPrice
      ) {
        setIsValidPrice(false);
      }
    }
  };

  const [name, setname] = React.useState("");

  const [phone, setPhone] = React.useState("");

  return (
    <>
      {isMobile ? (
        <>
          {cardDisplay === "donationMethod" ? (
            <PaymentMethod
              selectedPayment={selectedPaymentMethod}
              handleSelectedPayment={(value) => setSelectedPaymentMethod(value)}
              setDisplayCard={() => setCardDisplay("")}
            />
          ) : (
            <Grid
              style={{ padding: "0px 25px", paddingTop: 18, paddingBottom: 10 }}
            >
              <Typography
                style={{ fontWeight: 700, fontSize: 12, color: 666666 }}
              >
                Pilihan Donasi
              </Typography>
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              <Grid
                onClick={() => setCardDisplay("donationType")}
                style={{
                  display: "flex",
                  padding: "12px 5px",
                  marginBottom: 6,
                  cursor: "pointer",
                  width: "100%",
                  justifyContent: "start",
                }}
              >
                {/* {products.map((item) => ( */}
                {/* <> */}
                <Grid
                  item
                  // key={item.id}
                  xs={6}
                  style={{ alignSelf: "center" }}
                >
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <GreenRadio
                      checked={selectedProductId === "a"}
                      onChange={handleChangeProduct}
                      value="a"
                      name="radio-button-demo"
                      inputProps={{ "aria-label": "A" }}
                      style={{ padding: 0, paddingRight: 9 }}
                    />
                    <Typography
                      style={{
                        fontSize: 16,
                        fontWeight: 400,
                        textTransform: "uppercase",
                      }}
                    >
                      INFAK
                    </Typography>
                  </Grid>
                </Grid>

                <Grid
                  item
                  // key={item.id}
                  xs={6}
                  style={{ alignSelf: "center" }}
                >
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <GreenRadio
                      checked={selectedProductId === "b"}
                      onChange={handleChangeProduct}
                      value="b"
                      name="radio-button-demo"
                      inputProps={{ "aria-label": "B" }}
                      style={{ padding: 0, paddingRight: 9 }}
                    />
                    <Typography
                      style={{
                        fontSize: 16,
                        fontWeight: 400,
                        textTransform: "uppercase",
                      }}
                    >
                      ZAKAT
                    </Typography>
                  </Grid>
                </Grid>
                {/* </> */}
                {/* ))} */}
              </Grid>
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              <Grid style={{ display: "flex" }}>
                <Button
                  onClick={() => setCustomPrice(10000)}
                  style={{
                    padding: "3px 0px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 10.000
                  </Typography>
                </Button>

                <Button
                  onClick={() => setCustomPrice(25000)}
                  style={{
                    padding: "3px 0px",
                    margin: "0px 19px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 25.000
                  </Typography>
                </Button>

                <Button
                  onClick={() => setCustomPrice(50000)}
                  style={{
                    padding: "3px 0px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 50.000
                  </Typography>
                </Button>
              </Grid>
              <Grid
                style={{
                  padding: "8px 16px",
                  border: "1px solid #CCCCCC",
                  borderRadius: 2,
                  marginTop: 12,
                }}
              >
                <Grid
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 12,
                      color: "#4D4D4D",
                    }}
                  >
                    Nominal donasi
                  </Typography>
                </Grid>
                <Grid
                  style={{
                    display: "flex",
                    backgroundColor: "#F5F5F5",
                    padding: "7px 11px",
                    borderRadius: 2,
                    marginTop: 6,
                  }}
                >
                  <Typography
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                      backgroundColor: "#F5F5F5",
                    }}
                  >
                    Rp
                  </Typography>

                  <input
                    id="manig"
                    type="text"
                    // value={Constant.format(customPrice)}
                    value={customPrice.toLocaleString()}
                    onChange={(event) => {
                      const anjir = parseInt(
                        event.target.value.replace(/,/g, "")
                      );

                      if (anjir >= 1) {
                        event.target.value = anjir;
                        handleChangeCustomPrice(event);
                      } else {
                        setCustomPrice(0);
                      }
                    }}
                    style={{
                      padding: "4px 8px",
                      fontSize: 16,
                      fontWeight: 700,
                      color: 808080,
                      backgroundColor: "#F5F5F5",
                      border: "none",
                      outline: "none",
                      width: "100%",
                      textAlign: "end",
                    }}
                    placeholder={"0"}
                  />
                </Grid>
              </Grid>
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: 12,
                  color: 666666,
                  marginTop: 12,
                }}
              >
                Metode Pembayaran
              </Typography>

              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              <Button
                onClick={() => setCardDisplay("donationMethod")}
                style={{
                  display: "flex",
                  padding: 0,
                  paddingTop: 5,
                  paddingBottom: 18,
                  cursor: "pointer",
                  width: "100%",
                }}
              >
                <Grid
                  item
                  xs={6}
                  style={{
                    alignSelf: "center",
                  }}
                >
                  <Typography
                    style={{
                      fontSize: 12,
                      fontWeight: 400,
                      color: "#CCCCCC",
                      textAlign: "start",
                      textTransform: "capitalize",
                      whiteSpace: "nowrap",
                    }}
                  >
                    {selectedPaymentMethod ? (
                      <Typography style={{ color: "#4D4D4D", marginLeft: 45 }}>
                        {selectedPaymentMethod}
                      </Typography>
                    ) : (
                      "Pilih Metode Pembayaran"
                    )}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={6}
                  style={{ textAlign: "right", alignSelf: "center" }}
                  onClick={() => setCardDisplay("donationMethod")}
                >
                  {selectedPaymentMethod ? (
                    <Button
                      style={{
                        padding: "2px 12px",
                        background: "#35CF85",
                        borderRadius: 20,
                      }}
                    >
                      <Typography
                        style={{
                          fontSize: 12,
                          fontWeight: 600,
                          color: "rgba(250, 250, 250, 1)",
                          textTransform: "capitalize",
                        }}
                      >
                        Ganti
                      </Typography>
                      <KeyboardArrowDownRoundedIcon
                        style={{
                          color: "rgba(250, 250, 250, 1)",
                          marginLeft: 3,
                        }}
                      />
                    </Button>
                  ) : (
                    <ChevronRightRoundedIcon
                      style={{ color: "#CCCCCC", verticalAlign: "top" }}
                    />
                  )}
                </Grid>
              </Button>
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              <input
                type="text"
                style={{
                  padding: "14px 11px",
                  fontSize: 12,
                  fontWeight: 400,
                  backgroundColor: "#F5F5F5",
                  border: "1px solid #CCCCCC",
                  outline: "none",
                  width: "100%",
                  textAlign: "start",
                  borderRadius: 2,
                }}
                placeholder={"Masukkan Nama Lengkap"}
              />
              <input
                type="number"
                style={{
                  padding: "14px 11px",
                  marginTop: 12,
                  fontSize: 12,
                  fontWeight: 400,
                  backgroundColor: "#F5F5F5",
                  border: "1px solid #CCCCCC",
                  outline: "none",
                  width: "100%",
                  textAlign: "start",
                  borderRadius: 2,
                }}
                placeholder={"Masukkan Nomor Ponsel"}
              />
              <Grid
                style={{
                  display: "flex",
                  width: "100%",
                  alignItems: "center",
                  marginTop: 14,
                }}
              >
                <FormControlLabel
                  style={{ margin: 0 }}
                  control={
                    <GreenCheckbox
                      checked={state.checkedG}
                      onChange={handleChange}
                      name="checkedG"
                      fontSize="small"
                      style={{ padding: 0 }}
                    />
                  }
                />
                <Typography
                  style={{
                    fontSize: 11,
                    fontWeight: 700,
                    color: "#4D4D4D",
                    marginLeft: 8,
                  }}
                >
                  Donasi sebagai Hamba Allah
                </Typography>
              </Grid>
              <Button
                disabled={customPrice < 1}
                // onClick={() => setIsDisabled(true)}
                style={{
                  backgroundColor: customPrice < 1 ? "#B8B8B8" : "#2DBE78",
                  // backgroundColor: "#B8B8B8",
                  marginTop: 22,
                  marginBottom: 16,
                  width: "100%",
                  color: "#FAFAFA",
                  padding: 12,
                  textTransform: "inherit",
                  fontWeight: 600,
                }}
              >
                Donasi Sekarang
              </Button>
              <Typography
                style={{ fontSize: 9, fontWeight: 400, color: "#B8B8B8" }}
              >
                Dana yang didonasikan melalui InfakOnline bukan bersumber dan
                bukan untuk tujuan pencucian uang (money laundry), termasuk
                terorisme maupun tindak kejahatan lainnya.
              </Typography>
            </Grid>
          )}
        </>
      ) : (
        <Grid>
          {cardDisplay === "donationMethod" ? (
            <PaymentMethod
              selectedPayment={selectedPaymentMethod}
              handleSelectedPayment={(value) => setSelectedPaymentMethod(value)}
              setDisplayCard={() => setCardDisplay("")}
            />
          ) : (
            <Grid>
              <Typography
                style={{ fontWeight: 700, fontSize: 12, color: 666666 }}
              >
                Pilihan Donasi
              </Typography>
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              <Grid
                onClick={() => setCardDisplay("donationType")}
                style={{
                  display: "flex",
                  padding: "12px 5px",
                  marginBottom: 6,
                  cursor: "pointer",
                  width: "100%",
                  justifyContent: "start",
                }}
              >
                {/* {products.map((item) => ( */}
                {/* <> */}
                <Grid
                  item
                  // key={item.id}
                  xs={6}
                  style={{ alignSelf: "center" }}
                >
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <GreenRadio
                      checked={selectedProductId === "a"}
                      onChange={handleChangeProduct}
                      value="a"
                      name="radio-button-demo"
                      inputProps={{ "aria-label": "A" }}
                      style={{ padding: 0, paddingRight: 9 }}
                    />
                    <Typography
                      style={{
                        fontSize: 16,
                        fontWeight: 400,
                        textTransform: "uppercase",
                      }}
                    >
                      INFAK
                    </Typography>
                  </Grid>
                </Grid>

                <Grid
                  item
                  // key={item.id}
                  xs={6}
                  style={{ alignSelf: "center" }}
                >
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <GreenRadio
                      checked={selectedProductId === "b"}
                      onChange={handleChangeProduct}
                      value="b"
                      name="radio-button-demo"
                      inputProps={{ "aria-label": "B" }}
                      style={{ padding: 0, paddingRight: 9 }}
                    />
                    <Typography
                      style={{
                        fontSize: 16,
                        fontWeight: 400,
                        textTransform: "uppercase",
                      }}
                    >
                      ZAKAT
                    </Typography>
                  </Grid>
                </Grid>
                {/* </> */}
                {/* ))} */}
              </Grid>
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              {/* Donation Type */}
              <Grid style={{ display: "flex" }}>
                <Button
                  onClick={() => setCustomPrice(10000)}
                  style={{
                    padding: "3px 0px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 10.000
                  </Typography>
                </Button>

                <Button
                  onClick={() => setCustomPrice(25000)}
                  style={{
                    padding: "3px 0px",
                    margin: "0px 19px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 25.000
                  </Typography>
                </Button>

                <Button
                  onClick={() => setCustomPrice(50000)}
                  style={{
                    padding: "3px 0px",
                    border: "1px solid #CCCCCC",
                    borderRadius: 2,
                    width: "100%",
                    textTransform: "inherit",
                    display: "inherit",
                    alignItems: "none",
                    justifyContent: "none",
                  }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                    }}
                  >
                    Rp 50.000
                  </Typography>
                </Button>
              </Grid>
              <Grid
                style={{
                  padding: "8px 16px",
                  border: "1px solid #CCCCCC",
                  borderRadius: 2,
                  marginTop: 12,
                }}
              >
                <Grid
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <Typography
                    item
                    xs={6}
                    style={{
                      fontWeight: 700,
                      fontSize: 12,
                      color: "#4D4D4D",
                    }}
                  >
                    Nominal donasi
                  </Typography>
                </Grid>
                <Grid
                  style={{
                    display: "flex",
                    backgroundColor: "#F5F5F5",
                    padding: "7px 11px",
                    borderRadius: 2,
                    marginTop: 6,
                  }}
                >
                  <Typography
                    style={{
                      fontWeight: 700,
                      fontSize: 16,
                      color: "#4D4D4D",
                      backgroundColor: "#F5F5F5",
                    }}
                  >
                    Rp
                  </Typography>

                  <input
                    id="manig"
                    type="text"
                    // value={Constant.format(customPrice)}
                    value={customPrice.toLocaleString()}
                    onChange={(event) => {
                      const anjir = parseInt(
                        event.target.value.replace(/,/g, "")
                      );

                      if (anjir >= 1) {
                        event.target.value = anjir;
                        handleChangeCustomPrice(event);
                        console.log(
                          "ini handle custom bro",
                          handleChangeCustomPrice
                        );
                      } else {
                        setCustomPrice(0);
                      }
                    }}
                    // onChange={(event) => {
                    //   const anjir = parseInt(
                    //     event.target.value.replace(/,/g, "")
                    //   );
                    //   if (anjir >= 1) {
                    //     setCustomPrice(anjir);
                    //   } else {
                    //     setCustomPrice(0);
                    //   }
                    // }}
                    style={{
                      padding: "4px 8px",
                      fontSize: 16,
                      fontWeight: 700,
                      color: 808080,
                      backgroundColor: "#F5F5F5",
                      border: "none",
                      outline: "none",
                      width: "100%",
                      textAlign: "end",
                    }}
                    placeholder={"0"}
                  />
                </Grid>
                {!isValidPrice &&
                  `Nominal tidak boleh kurang dari ${selectedProduct.customPriceConfig.minimumCustomPrice}!`}
              </Grid>
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: 12,
                  color: 666666,
                  marginTop: 12,
                }}
              >
                Metode Pembayaran
              </Typography>

              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              <Button
                onClick={() => setCardDisplay("donationMethod")}
                style={{
                  display: "flex",
                  padding: "14px 5px",
                  borderTop: "1px solid #CCCCCC",
                  borderBottom: "1px solid #CCCCCC",
                  marginTop: 11,
                  marginBottom: 15,
                  cursor: "pointer",
                  width: "100%",
                }}
              >
                <Grid
                  item
                  xs={6}
                  style={{ alignSelf: "center", display: "flex" }}
                >
                  <Typography
                    style={{
                      fontSize: 14,
                      fontWeight: 400,
                      color: "#CCCCCC",
                      textAlign: "start",
                      textTransform: "capitalize",
                      whiteSpace: "nowrap",
                    }}
                  >
                    {selectedPaymentMethod ? (
                      <Typography style={{ color: "#4D4D4D", marginLeft: 45 }}>
                        {selectedPaymentMethod}
                      </Typography>
                    ) : (
                      "Pilih Metode Pembayaran"
                    )}
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={6}
                  style={{ textAlign: "right", alignSelf: "center" }}
                  onClick={() => setCardDisplay("donationMethod")}
                >
                  {selectedPaymentMethod ? (
                    <Button
                      style={{
                        padding: "2px 12px",
                        background: "#35CF85",
                        borderRadius: 20,
                      }}
                    >
                      <Typography
                        style={{
                          fontSize: 12,
                          fontWeight: 600,
                          color: "rgba(250, 250, 250, 1)",
                          textTransform: "capitalize",
                        }}
                      >
                        Ganti
                      </Typography>
                      <KeyboardArrowDownRoundedIcon
                        style={{
                          color: "rgba(250, 250, 250, 1)",
                          marginLeft: 3,
                        }}
                      />
                    </Button>
                  ) : (
                    <ChevronRightRoundedIcon
                      style={{ color: "#CCCCCC", verticalAlign: "top" }}
                    />
                  )}
                </Grid>
              </Button>
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              {/* Payment Method */}
              <input
                type="text"
                onChange={(event) => {
                  setname(event.target.value);
                }}
                style={{
                  padding: "11px 12px",
                  fontSize: 11,
                  fontWeight: 400,
                  backgroundColor: "#F5F5F5",
                  border: "1px solid #CCCCCC",
                  outline: "none",
                  width: "100%",
                  textAlign: "start",
                  borderRadius: 2,
                }}
                placeholder={"Masukkan Nama Lengkap"}
              />
              <input
                type="number"
                onChange={(event) => {
                  setPhone(event.target.value);
                }}
                style={{
                  padding: "11px 12px",
                  marginTop: 12,
                  fontSize: 11,
                  fontWeight: 400,
                  backgroundColor: "#F5F5F5",
                  border: "1px solid #CCCCCC",
                  outline: "none",
                  width: "100%",
                  textAlign: "start",
                  borderRadius: 2,
                }}
                placeholder={"Masukkan Nomor Ponsel"}
              />
              <Grid
                style={{
                  display: "flex",
                  width: "100%",
                  alignItems: "center",
                  marginTop: 14,
                }}
              >
                <FormControlLabel
                  style={{ margin: 0 }}
                  control={
                    <GreenCheckbox
                      checked={state.checkedG}
                      onChange={handleChange}
                      name="checkedG"
                      fontSize="small"
                      style={{ padding: 0 }}
                    />
                  }
                />
                <Typography
                  style={{
                    fontSize: 12,
                    fontWeight: 700,
                    color: "#4D4D4D",
                    marginLeft: 8,
                  }}
                >
                  Donasi sebagai Hamba Allah
                </Typography>
              </Grid>
              <Button
                disabled={customPrice < 1 || !name || !phone}
                onClick={handleSubmit}
                style={{
                  backgroundColor:
                    customPrice < 1 || !name || !phone ? "#B8B8B8" : "#2DBE78",
                  // backgroundColor: "#B8B8B8",
                  marginTop: 22,
                  marginBottom: 16,
                  width: "100%",
                  color: "#FAFAFA",
                  padding: 12,
                  textTransform: "inherit",
                  fontWeight: 600,
                }}
              >
                Donasi Sekarang
              </Button>
              <Typography
                style={{ fontSize: 8, fontWeight: 400, color: "#B8B8B8" }}
              >
                Dana yang didonasikan melalui InfakOnline bukan bersumber dan
                bukan untuk tujuan pencucian uang (money laundry), termasuk
                terorisme maupun tindak kejahatan lainnya.
              </Typography>
            </Grid>
          )}
        </Grid>
      )}
    </>
  );
}

export default DonationCard;
