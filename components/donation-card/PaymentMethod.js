import React from "react";
import { Grid, Typography } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { withStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const GreenCheckbox = withStyles({
  root: {
    display: "none",
    "&$checked": {
      display: "flex",
      color: "#2DBE78",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

function PaymentMethod(props) {
  const defaultValue = {
    GoPay: false,
    DANA: false,
    ShoppeePay: false,
    LinkAja: false,
    JeniusPay: false,
    BCA: false,
    Mandiri: false,
    BNI: false,
    BRI: false,
    BSM: false,
  };
  const [state, setState] = React.useState({
    ...defaultValue,
  });
  const handleChange = (event) => {
    setState({
      ...defaultValue,
      [event.target.name]: event.target.checked,
    });
  };
  const data = [
    {
      id: 0,
      name: "Go-PAY",
      state: "GoPay",
    },
    {
      id: 1,
      name: "DANA",
      state: "DANA",
    },
    {
      id: 2,
      name: "ShopeePay",
      state: "ShopeePay",
    },
    {
      id: 3,
      name: "LinkAja",
      state: "LinkAja",
    },
    {
      id: 4,
      name: "JeniusPay",
      state: "JeniusPay",
    },
  ];
  const data2 = [
    {
      id: 0,
      name: "BCA Virtual Account",
      state: "BCA",
    },
    {
      id: 1,
      name: "Mandiri Virtual Account",
      state: "Mandiri",
    },
    {
      id: 2,
      name: "BNI Virtual Account",
      state: "BNI",
    },
    {
      id: 3,
      name: "BRI Virtual Account",
      state: "BRI GoPay",
    },
    {
      id: 4,
      name: "BSM Virtual Account",
      state: "BSM GoPay",
    },
  ];

  const handleClick = (dataName, dataValue) => {
    if (!dataValue) {
      props.handleSelectedPayment(null);
      props.setDisplayCard();
    } else {
      props.handleSelectedPayment(dataName);
      props.setDisplayCard();
    }
    setState({
      ...defaultValue,
      [dataName]: dataValue,
    });
  };

  React.useEffect(() => {
    setState({
      ...state,
      [props.selectedPayment]: true,
    });
  }, []);

  // console.log(props.haris);
  return (
    <Grid style={{ padding: "35px 20px" }}>
      <Grid
        onClick={() => props.setDisplayCard()}
        style={{ display: "flex", alignItems: "center", cursor: "pointer" }}
      >
        <ArrowBackIcon />
        <Typography style={{ fontSize: 14, fontWeight: 700, marginLeft: 8 }}>
          Kembali
        </Typography>
      </Grid>

      <Grid>
        <Grid
          style={{
            marginTop: 12,
            padding: "6px 11px",
            background: "#F2F3F4",
            width: "100%",
            borderRadius: 2,
          }}
        >
          <Typography
            style={{ fontSize: 12, fontWeight: 700, color: "#4D4D4D" }}
          >
            Pembayaran Instan (Cepat dan Mudah)
          </Typography>
        </Grid>
        <Grid style={{ padding: "0px 12px" }}>
          <Grid
            onClick={() => {
              handleClick("GoPay", !state.GoPay);
              // console.log("GoPay", !state.GoPay);
              // console.log("akowkowkokwoko");
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                }}
              >
                Go-PAY
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.GoPay}
                    // onChange={handleChange}
                    name="GoPay"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("DANA", !state.DANA);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                }}
              >
                DANA
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.DANA}
                    onChange={handleChange}
                    name="DANA"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("ShoppeePay", !state.ShoppeePay);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                }}
              >
                ShoppeePay
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.ShoppeePay}
                    onChange={handleChange}
                    name="ShoppeePay"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("LinkAja", !state.LinkAja);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                }}
              >
                LinkAja
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.LinkAja}
                    onChange={handleChange}
                    name="LinkAja"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("JeniusPay", !state.JeniusPay);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                }}
              >
                JeniusPay
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.JeniusPay}
                    onChange={handleChange}
                    name="JeniusPay"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Grid>
        <Grid
          style={{
            marginTop: 20,
            padding: "6px 11px",
            background: "#F2F3F4",
            width: "100%",
            borderRadius: 2,
          }}
        >
          <Typography
            style={{ fontSize: 12, fontWeight: 700, color: "#4D4D4D" }}
          >
            Virtual Account (Verifikasi Otomatis)
          </Typography>
        </Grid>
        <Grid style={{ padding: "0px 12px" }}>
          <Grid
            onClick={() => {
              handleClick("BCA", !state.BCA);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                  whiteSpace: "nowrap",
                }}
              >
                BCA Virtual Account
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.BCA}
                    onChange={handleChange}
                    name="BCA"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("Mandiri", !state.Mandiri);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,

                  whiteSpace: "nowrap",
                }}
              >
                Mandiri Virtual Account
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.Mandiri}
                    onChange={handleChange}
                    name="Mandiri"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("BNI", !state.BNI);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                  whiteSpace: "nowrap",
                }}
              >
                BNI Virtual Account
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.BNI}
                    onChange={handleChange}
                    name="BNI"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("BRI", !state.BRI);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              marginBottom: 16,
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                  whiteSpace: "nowrap",
                }}
              >
                BRI Virtual Account
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.BRI}
                    onChange={handleChange}
                    name="BRI"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
          <Grid
            onClick={() => {
              handleClick("BSM", !state.BSM);
            }}
            style={{
              display: "flex",
              width: "100%",
              padding: "18px 0px",
              marginBottom: 16,
              alignItems: "center",
              borderBottom: "1px solid #CCCCCC",
              cursor: "pointer",
            }}
          >
            <Grid item xs={6}>
              <Typography
                style={{
                  fontSize: 14,
                  fontWeight: 400,
                  paddingLeft: 55,
                  whiteSpace: "nowrap",
                }}
              >
                BSM Virtual Account
              </Typography>
            </Grid>
            <Grid
              item
              xs={6}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <FormControlLabel
                style={{ margin: 0 }}
                control={
                  <GreenCheckbox
                    checked={state.BSM}
                    onChange={handleChange}
                    name="BSM"
                    fontSize="small"
                    style={{ padding: 0 }}
                  />
                }
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default PaymentMethod;
