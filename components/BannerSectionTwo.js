import { Grid, Typography } from "@material-ui/core";
import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import { useMediaQuery } from "react-responsive";
import Carousel from "../components/Carousel";
import CarouselResp from "../components/CarouselResp";

function BannerSectionTwo() {
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  const data = [
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
  ];
  const data2 = [
    {
      icon: "/Icon/IconBannerTwo-1.svg",
      name: "Mudah",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare nec sit morbi imperdiet convallis mattis eget.",
    },
    {
      icon: "/Icon/IconBannerTwo-2.svg",
      name: "Beragam",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare nec sit morbi imperdiet convallis mattis eget.",
    },
    {
      icon: "/Icon/IconBannerTwo-3.svg",
      name: "Aman",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare nec sit morbi imperdiet convallis mattis eget.",
    },
    {
      icon: "/Icon/IconBannerTwo-4.svg",
      name: "Terbuka",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare nec sit morbi imperdiet convallis mattis eget.",
    },
  ];

  const settings = {
    dots: true,
    infinite: true,
    // autoplay: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  return (
    <>
      {isMob ? (
        <>
          <>
            <Grid
              style={{
                margin: "30px 20px",
              }}
            >
              <Grid>
                <Typography
                  style={{ fontSize: 28, fontWeight: 700, color: "#4D4D4D" }}
                >
                  Infakonline
                </Typography>

                <Grid
                  style={{
                    // backgroundImage: "url(/Frame/Phone.svg)",
                    backgroundSize: "contain",
                    backgroundRepeat: "no-repeat",
                    backgroundPositionX: "right",
                  }}
                >
                  <Grid
                    style={{
                      paddingTop: 33,
                    }}
                  >
                    {data2.map((item) => (
                      <Grid style={{ display: "flex", marginBottom: 25 }}>
                        <Grid>
                          <img src={item.icon} style={{ width: 45 }} />
                        </Grid>
                        <Grid
                          style={{
                            marginTop: 8,
                            marginLeft: 16,
                          }}
                        >
                          <Typography
                            style={{
                              fontSize: 20,
                              fontWeight: 600,
                              margin: 0,
                            }}
                          >
                            {item.name}
                          </Typography>
                          <Typography
                            style={{
                              fontSize: 13,
                              fontWeight: 400,
                              margin: 0,
                              marginTop: 13,
                            }}
                          >
                            {item.desc}
                          </Typography>
                        </Grid>
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid style={{ marginTop: 57 }}>
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: 28,
                  marginBottom: 41,
                  color: "#4D4D4D",
                  marginLeft: 7,
                }}
              >
                Berita Hari Ini
              </Typography>
              <Grid>
                {data.map((item) => (
                  <Grid style={{ margin: "0px 7px", marginBottom: 24 }}>
                    <Card>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt="Mecca-FHD"
                          // height="140"
                          image={item.img}
                          title="Beautiful Mecca"
                        />
                        <CardContent>
                          <Grid style={{ display: "flex" }}>
                            <Grid
                              style={{
                                display: "flex",
                                alignItems: "center",
                              }}
                            >
                              <Grid style={{ display: "flex" }}>
                                <img src="/Icon/Calendar.svg" />
                              </Grid>
                              <Typography
                                style={{
                                  fontSize: 14,
                                  fontWeight: 400,
                                  marginLeft: 9,
                                  whiteSpace: "nowrap",
                                }}
                              >
                                30 Sep 2018
                              </Typography>
                            </Grid>
                            <Grid
                              style={{
                                display: "flex",
                                alignItems: "center",
                                marginLeft: 30,
                              }}
                            >
                              <Grid style={{ display: "flex" }}>
                                <img src="/Icon/Person.svg" />
                              </Grid>
                              <Typography
                                style={{
                                  fontSize: 14,
                                  fontWeight: 400,
                                  marginLeft: 9,
                                  whiteSpace: "nowrap",
                                }}
                              >
                                Abdul Karim
                              </Typography>
                            </Grid>
                          </Grid>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 600,
                              color: "#1A1A1A",
                            }}
                          >
                            Promotional Advertising Specialty You Ve Waited Long
                            Enough
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </Grid>
                ))}
                <Typography
                  style={{
                    fontSize: 16,
                    fontWeight: 700,
                    color: "#28A96B",
                    textAlign: "center",
                    marginTop: 3,
                    marginBottom: 27,
                  }}
                >
                  Lihat Semua
                </Typography>
              </Grid>
            </Grid>

            <Grid
              style={{
                background: "rgba(94, 217, 158, 0.3)",
                padding: "20px 30px",
              }}
            >
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: 24,
                  color: "#4D4D4D",
                }}
              >
                Partner kami
              </Typography>
              <Grid
                style={{
                  margin: "34px 0",
                  // paddingTop: 61,
                  justifyContent: "center",
                }}
              >
                <CarouselResp />
                {/* <Grid style={{ width: "37%" }} /> */}
              </Grid>
            </Grid>
          </>
        </>
      ) : (
        <>
          <>
            <Grid
              style={{
                margin: "33px 0px",
                marginLeft: 106,
              }}
            >
              <Grid>
                <Typography
                  style={{ fontSize: 28, fontWeight: 700, color: "#4D4D4D" }}
                >
                  Infakonline
                </Typography>

                <Grid
                  style={{
                    backgroundImage: "url(/Frame/Phone.svg)",
                    // backgroundSize: "contain",
                    backgroundSize: "63%",
                    backgroundRepeat: "no-repeat",
                    backgroundPositionX: "right",
                    paddingBottom: "19%",
                  }}
                >
                  <Grid
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      paddingTop: 49,
                      width: "92%",
                    }}
                  >
                    <Grid style={{ width: "63%" }}>
                      <Grid style={{ display: "flex" }}>
                        <Grid itsm xs={6} style={{ display: "flex" }}>
                          <Grid>
                            <img src="/Icon/IconBannerTwo-1.svg" />
                          </Grid>
                          <Grid
                            style={{
                              alignItems: "center",
                              marginLeft: 19,
                              marginTop: 9,
                            }}
                          >
                            <Typography
                              style={{
                                fontSize: 20,
                                fontWeight: 600,
                                margin: 0,
                              }}
                            >
                              Mudah
                            </Typography>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                margin: 0,
                                marginTop: 15,
                              }}
                            >
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit. Ornare nec sit morbi imperdiet convallis
                              mattis eget.
                            </Typography>
                          </Grid>
                        </Grid>
                        <Grid
                          itsm
                          xs={6}
                          style={{ display: "flex", marginLeft: 45 }}
                        >
                          <Grid>
                            <img src="/Icon/IconBannerTwo-2.svg" />
                          </Grid>
                          <Grid
                            style={{
                              alignItems: "center",
                              marginLeft: 19,
                              marginTop: 9,
                            }}
                          >
                            <Typography
                              style={{
                                fontSize: 20,
                                fontWeight: 600,
                                margin: 0,
                              }}
                            >
                              Beragam
                            </Typography>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                margin: 0,
                                marginTop: 15,
                              }}
                            >
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit. Ornare nec sit morbi imperdiet convallis
                              mattis eget.
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid style={{ display: "flex", marginTop: 70 }}>
                        <Grid itsm xs={6} style={{ display: "flex" }}>
                          <Grid>
                            <img src="/Icon/IconBannerTwo-3.svg" />
                          </Grid>
                          <Grid
                            style={{
                              alignItems: "center",
                              marginLeft: 19,
                              marginTop: 9,
                            }}
                          >
                            <Typography
                              style={{
                                fontSize: 20,
                                fontWeight: 600,
                                margin: 0,
                              }}
                            >
                              Aman
                            </Typography>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                margin: 0,
                                marginTop: 15,
                              }}
                            >
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit. Ornare nec sit morbi imperdiet convallis
                              mattis eget.
                            </Typography>
                          </Grid>
                        </Grid>
                        <Grid
                          itsm
                          xs={6}
                          style={{ display: "flex", marginLeft: 45 }}
                        >
                          <Grid>
                            <img src="/Icon/IconBannerTwo-4.svg" />
                          </Grid>
                          <Grid
                            style={{
                              alignItems: "center",
                              marginLeft: 19,
                              marginTop: 9,
                            }}
                          >
                            <Typography
                              style={{
                                fontSize: 20,
                                fontWeight: 600,
                                margin: 0,
                              }}
                            >
                              Terbuka
                            </Typography>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                margin: 0,
                                marginTop: 15,
                              }}
                            >
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit. Ornare nec sit morbi imperdiet convallis
                              mattis eget.
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid style={{ width: "37%" }} />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid style={{ margin: "33px 99px" }}>
              <Grid>
                <Typography
                  style={{
                    fontWeight: 700,
                    fontSize: 28,
                    marginBottom: 41,
                    color: "#4D4D4D",
                    marginLeft: 7,
                  }}
                >
                  Berita Hari Ini
                </Typography>
                <Grid
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  {data.map((item) => (
                    <Grid item xs={4} style={{ margin: "0px 7px" }}>
                      <Card>
                        <CardActionArea>
                          <CardMedia
                            component="img"
                            alt="Mecca-FHD"
                            // height="140"
                            image={item.img}
                            title="Beautiful Mecca"
                          />
                          <CardContent>
                            <Grid style={{ display: "flex" }}>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Calendar.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  30 Sep 2018
                                </Typography>
                              </Grid>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  marginLeft: 30,
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Person.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Abdul Karim
                                </Typography>
                              </Grid>
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 600,
                                color: "#1A1A1A",
                              }}
                            >
                              Promotional Advertising Specialty You Ve Waited
                              Long Enough
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>

            <Grid
              style={{
                background: "rgba(94, 217, 158, 0.3)",
                paddingTop: 33,
                marginTop: 74,
              }}
            >
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: 28,
                  color: "#4D4D4D",
                  padding: "0px 106px",
                }}
              >
                Partner kami
              </Typography>
              <Carousel />
            </Grid>
          </>
        </>
      )}
    </>
  );
}

export default BannerSectionTwo;
