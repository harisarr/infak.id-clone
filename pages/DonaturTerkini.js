import React from "react";
import { Grid, Typography } from "@material-ui/core";
import Image from "next/image";
import styles from "./styles.module.css";
import { useMediaQuery } from "react-responsive";
import { Spin } from "antd";
import "antd/dist/antd.css";

function DonaturTerkini() {
  const data = [
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Hamba Allah",
      via: "Gopay",
      nominal: "Rp 5.000",
      time: "1 menit lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
    {
      donatur: "Susiyanti",
      via: "Ovo",
      nominal: "Rp 5.000",
      time: "30 menit lalu",
    },
    {
      donatur: "Joko Susilo",
      via: "Bank BCA",
      nominal: "Rp 50.000",
      time: "1 jam lalu",
    },
  ];

  const [isMobile, setisMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setisMobile(isMob);
    } else {
      setisMobile(false);
    }
  }, [isMob]);
  return (
    <>
      {isMobile ? (
        isLoading ? (
          <div
            style={{
              minHeight: "100vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        ) : (
          <Grid style={{ paddingTop: 52 }}>
            <Grid>
              <Grid
                style={{
                  margin: "19px 8px",
                  background: "white",
                  borderRadius: 8,
                  boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
                }}
              >
                <Grid style={{ padding: "7px 9px" }}>
                  <Grid
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Grid item xs={6}>
                      <Typography
                        style={{
                          fontSize: 9,
                          fontWeight: 600,
                          color: "#4d4d4d",
                          whiteSpace: "nowrap",
                        }}
                      >
                        Jumlah Infak terkumpul
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      style={{ display: "flex", justifyContent: "flex-end" }}
                    >
                      <Image src="/Icon/Verified.svg" width={10} height={10} />
                      <Typography
                        style={{
                          fontWeight: 600,
                          fontSize: 9,
                          color: "#2DBE78",
                          marginLeft: 9,
                        }}
                      >
                        345
                      </Typography>
                    </Grid>
                  </Grid>
                  <Typography
                    style={{
                      fontSize: 28,
                      fontWeight: 700,
                      paddingLeft: 10,
                    }}
                  >
                    Rp 201.140.000
                  </Typography>
                </Grid>

                <div
                  style={{
                    border: "1px solid #F5F5F5",
                    width: "100%",
                  }}
                />

                <Grid style={{ padding: "7px 9px" }}>
                  <Grid style={{ display: "flex" }}>
                    <Typography
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        fontWeight: 700,
                        fontSize: 10,
                        color: "#4D4D4D",
                        width: "50%",
                      }}
                    >
                      Donatur Terkini
                    </Typography>
                  </Grid>

                  <Grid
                    style={{
                      display: "flex",
                      border: "1px solid #2DBE78",
                      borderRadius: 4,
                      marginBottom: 3,
                    }}
                  >
                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#2DBE78",
                          color: "#FAFAFA",
                          alignItems: "center",
                          fontSize: 9,
                          fontWeight: 600,
                          padding: "2px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Donatur
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 9,
                          fontWeight: 600,
                          padding: "2px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Via
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 9,
                          fontWeight: 600,
                          padding: "2px 18px",
                          borderRadius: 2,
                          textAlign: "center",
                        }}
                      >
                        Nominal
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <Typography
                        style={{
                          background: "#FAFAFA",
                          color: "#2DBE78",
                          alignItems: "center",
                          fontSize: 9,
                          fontWeight: 600,
                          padding: "2px 0px",
                          borderRadius: 2,
                          textAlign: "center",
                          whiteSpace: "nowrap",
                        }}
                      >
                        Waktu Donasi
                      </Typography>
                    </Grid>
                  </Grid>

                  <Grid
                    style={{
                      overflowY: "scroll",
                      overflowX: "hidden",
                      height: 360,
                    }}
                    className={styles.scrollBar}
                  >
                    {data.map((item) => (
                      <Grid
                        style={{ display: "flex", paddingTop: 5 }}
                        className={styles.HoverList}
                      >
                        <Grid item xs={4} style={{ alignSelf: "center" }}>
                          <Typography
                            style={{
                              background: "#FFFFF",
                              color: "#4D4D4D",
                              fontSize: 9,
                              fontWeight: 600,
                              textAlign: "start",
                              paddingLeft: 15,
                            }}
                          >
                            {item.donatur}
                          </Typography>
                        </Grid>
                        <Grid item xs={4} style={{ alignSelf: "center" }}>
                          <Typography
                            style={{
                              background: "#FFFFF",
                              color: "#4D4D4D",
                              fontSize: 9,
                              fontWeight: 600,
                              textAlign: "start",
                              paddingLeft: 15,
                            }}
                          >
                            {item.via}
                          </Typography>
                        </Grid>
                        <Grid item xs={4} style={{ alignSelf: "center" }}>
                          <Typography
                            style={{
                              background: "#FFFFF",
                              color: "#4D4D4D",
                              fontSize: 9,
                              fontWeight: 600,
                              textAlign: "start",
                              paddingLeft: 15,
                            }}
                          >
                            {item.nominal}
                          </Typography>
                        </Grid>
                        <Grid item xs={4} style={{ alignSelf: "center" }}>
                          <Typography
                            style={{
                              background: "#FFFFF",
                              color: "#4D4D4D",
                              fontSize: 9,
                              fontWeight: 600,
                              textAlign: "start",
                              paddingLeft: 15,
                            }}
                          >
                            {item.time}
                          </Typography>
                        </Grid>
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )
      ) : (
        <Grid style={{ padding: 0, paddingTop: 72 }}>
          {isLoading ? (
            <div
              style={{
                minHeight: "100vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                zIndex: 9999,
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <Grid style={{ margin: "0px 166px", marginTop: 35 }}>
              <Grid
                style={{
                  background: "white",
                  borderRadius: 8,
                  padding: 14,
                  boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
                  // height: 570,
                  marginBottom: 50,
                }}
              >
                <Grid style={{ display: "flex" }}>
                  <Grid>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontWeight: 600,
                        color: "#4d4d4d",
                        whiteSpace: "nowrap",
                      }}
                    >
                      Jumlah Infak terkumpul
                    </Typography>
                  </Grid>
                </Grid>
                <Grid style={{ display: "flex" }}>
                  <Grid item xs={6}>
                    <Typography
                      style={{
                        fontSize: 40,
                        fontWeight: 700,
                        marginLeft: 17,
                        marginTop: 8,
                      }}
                    >
                      Rp 201.140.000
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Image src="/Icon/Verified.svg" width={21} height={19} />
                    <Typography
                      style={{
                        fontWeight: 600,
                        fontSize: 21,
                        color: "#2DBE78",
                        marginLeft: 9,
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      345
                    </Typography>
                  </Grid>
                </Grid>

                <div
                  style={{
                    border: "1px solid #F5F5F5",
                    width: "100%",
                    marginTop: 25,
                    marginBottom: 11,
                  }}
                />

                <Grid style={{ display: "flex" }}>
                  <Typography
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                      fontWeight: 700,
                      fontSize: 14,
                      color: "#4D4D4D",
                      width: "50%",
                    }}
                  >
                    Donatur Terkini
                  </Typography>
                </Grid>

                <Grid
                  style={{
                    display: "flex",
                    border: "1px solid #2DBE78",
                    borderRadius: 4,
                  }}
                >
                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#2DBE78",
                        color: "#FAFAFA",
                        alignItems: "center",
                        fontSize: 12,
                        fontWeight: 600,
                        padding: "7px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Donatur
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 12,
                        fontWeight: 600,
                        padding: "7px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Via
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 12,
                        fontWeight: 600,
                        padding: "7px 18px",
                        borderRadius: 2,
                        textAlign: "center",
                      }}
                    >
                      Nominal
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography
                      style={{
                        background: "#FAFAFA",
                        color: "#2DBE78",
                        alignItems: "center",
                        fontSize: 12,
                        fontWeight: 600,
                        padding: "7px 0px",
                        borderRadius: 2,
                        textAlign: "center",
                        whiteSpace: "nowrap",
                      }}
                    >
                      Waktu Donasi
                    </Typography>
                  </Grid>
                </Grid>

                <Grid
                  style={{
                    overflowY: "scroll",
                    overflowX: "hidden",
                    height: 360,
                  }}
                >
                  {data.map((item) => (
                    <Grid
                      style={{ display: "flex", paddingTop: 8 }}
                      className={styles.HoverList}
                    >
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.donatur}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.via}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.nominal}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          style={{
                            background: "#FFFFF",
                            color: "#4D4D4D",
                            fontSize: 12,
                            fontWeight: 600,
                            padding: "0px 18px",
                            textAlign: "start",
                          }}
                        >
                          {item.time}
                        </Typography>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      )}

      {/* <Footer /> */}
    </>
  );
}

export default DonaturTerkini;
