import Head from "next/head";
import "../styles/globals.css";
import React from "react";
import "../styles/globals.css";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../theme";
import NavbarMain from "../components/NavbarMain";
import NavbarSecond from "../components/NavbarSecond";
import { useRouter } from "next/router";
import Footer from "../components/Footer";

export default function MyApp({ Component, pageProps }) {
  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const router = useRouter();

  return (
    <React.Fragment>
      <Head>
        <title>InfakOnline.id</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <ThemeProvider theme={theme}>
        {/* <div style={{ minHeight: "100%", maxWidth: 9999 }}> */}
        {router.pathname === "/" ? <NavbarMain /> : <NavbarSecond />}
        <CssBaseline />
        <Component {...pageProps} />
        <div style={{ position: "relative", bottom: 0 }}>
          <Footer />
        </div>
        {/* </div> */}
      </ThemeProvider>
    </React.Fragment>
  );
}
