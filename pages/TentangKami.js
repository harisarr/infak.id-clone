import React from "react";
import { useMediaQuery } from "react-responsive";
import { Grid, Typography } from "@material-ui/core";
import Image from "next/image";
import styles from "../components/styles.module.css";
import { Spin } from "antd";
import "antd/dist/antd.css";

function TentangKami() {
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  const data = [
    {
      img: "/Frame/Nabawi-Wallpaper.jpg",
    },
    {
      img: "/Frame/Nabawi-Wallpaper.jpg",
    },
    {
      img: "/Frame/Nabawi-Wallpaper.jpg",
    },
  ];

  return (
    <>
      {isMobile ? (
        isLoading ? (
          <div
            style={{
              minHeight: "100vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        ) : (
          <Grid style={{ padding: "53px 20px" }}>
            <Typography
              style={{
                fontSize: 25,
                fontWeight: 600,
                marginBottom: 20,
                color: "#2DBE78",
                textAlign: "center",
                textDecorationLine: "underline",
              }}
            >
              Tentang Kami
            </Typography>
            <Grid>
              <Grid>
                <Grid style={{ display: "flex", marginBottom: 10 }}>
                  <Grid>
                    <img
                      src="/Icon/IconBannerTwo-1.svg"
                      style={{ width: 45 }}
                    />
                  </Grid>
                  <Grid
                    style={{
                      alignItems: "center",
                      marginLeft: 19,
                      marginTop: 9,
                    }}
                  >
                    <Typography
                      style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                    >
                      Mudah
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        margin: 0,
                        marginTop: 5,
                        color: "#595959",
                      }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ornare nec sit morbi imperdiet convallis mattis eget.
                    </Typography>
                  </Grid>
                </Grid>
                <Grid style={{ display: "flex", marginBottom: 10 }}>
                  <Grid>
                    <img
                      src="/Icon/IconBannerTwo-2.svg"
                      style={{ width: 45 }}
                    />
                  </Grid>
                  <Grid
                    style={{
                      alignItems: "center",
                      marginLeft: 19,
                      marginTop: 9,
                    }}
                  >
                    <Typography
                      style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                    >
                      Beragam
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        margin: 0,
                        marginTop: 5,
                        color: "#595959",
                      }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ornare nec sit morbi imperdiet convallis mattis eget.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid>
                <Grid style={{ display: "flex", marginBottom: 10 }}>
                  <Grid>
                    <img
                      src="/Icon/IconBannerTwo-3.svg"
                      style={{ width: 45 }}
                    />
                  </Grid>
                  <Grid
                    style={{
                      alignItems: "center",
                      marginLeft: 19,
                      marginTop: 9,
                    }}
                  >
                    <Typography
                      style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                    >
                      Aman
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        margin: 0,
                        marginTop: 5,
                        color: "#595959",
                      }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ornare nec sit morbi imperdiet convallis mattis eget.
                    </Typography>
                  </Grid>
                </Grid>
                <Grid style={{ display: "flex", marginBottom: 10 }}>
                  <Grid>
                    <img
                      src="/Icon/IconBannerTwo-4.svg"
                      style={{ width: 45 }}
                    />
                  </Grid>
                  <Grid
                    style={{
                      alignItems: "center",
                      marginLeft: 19,
                      marginTop: 9,
                    }}
                  >
                    <Typography
                      style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                    >
                      Terbuka
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        margin: 0,
                        marginTop: 5,
                        color: "#595959",
                      }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ornare nec sit morbi imperdiet convallis mattis eget.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Grid style={{ margin: "50px 0px" }}>
              <Image src="/Icon/Phone.svg" width={500} height={302} />
            </Grid>

            <Typography
              style={{ fontSize: 13, fontWeight: 400, color: "#595959" }}
            >
              Leverage agile frameworks to provide a robust synopsis for high
              level overviews. Iterative approaches to corporate strategy foster
              collaborative thinking to further the overall value proposition.
              Organically grow the holistic world view of disruptive innovation
              via workplace diversity and empowerment. Bring to the table
              win-win survival strategies to ensure proactive domination. At the
              end of the day, going forward, a new normal that has evolved from
              generation X is on the runway heading towards a streamlined cloud
              solution. User generated content in real-time will have multiple
              touchpoints for offshoring.
            </Typography>

            <Grid
              style={{
                display: "block",
                marginTop: 25,
                marginBottom: 25,
                width: "100%",
              }}
            >
              <Image
                src="/Frame/Nabawi-Wallpaper.jpg"
                width={500}
                height={302}
                className={styles.TentangKamiImg}
              />
            </Grid>
            <Typography
              style={{ fontSize: 13, fontWeight: 400, color: "#595959" }}
            >
              At the end of the day, going forward, a new normal that has
              evolved from generation X is on the runway heading towards a
              streamlined cloud solution. User generated content in real-time
              will have multiple touchpoints for offshoring. Capitalize on low
              hanging fruit to identify a ballpark value added activity to beta
              test.
            </Typography>
          </Grid>
        )
      ) : (
        <>
          {isLoading ? (
            <div
              style={{
                minHeight: "100vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                zIndex: 9999,
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <Grid style={{ paddingTop: 75 }}>
              <Grid style={{ margin: "53px 295px 0px" }}>
                <Typography
                  style={{
                    fontSize: 28,
                    fontWeight: 600,
                    marginBottom: 35,
                    color: "#2DBE78",
                    textAlign: "center",
                    textDecorationLine: "underline",
                  }}
                >
                  Tentang Kami
                </Typography>
                <Grid style={{ marginTop: 60 }}>
                  <Grid
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Grid style={{ display: "flex", width: 360 }}>
                      <Grid>
                        <img src="/Icon/IconBannerTwo-1.svg" />
                      </Grid>
                      <Grid
                        style={{
                          alignItems: "center",
                          marginLeft: 19,
                          marginTop: 9,
                        }}
                      >
                        <Typography
                          style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                        >
                          Mudah
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 14,
                            fontWeight: 400,
                            margin: 0,
                            marginTop: 15,
                          }}
                        >
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Ornare nec sit morbi imperdiet convallis mattis
                          eget.
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid style={{ display: "flex", width: 360 }}>
                      <Grid>
                        <img src="/Icon/IconBannerTwo-2.svg" />
                      </Grid>
                      <Grid
                        style={{
                          alignItems: "center",
                          marginLeft: 19,
                          marginTop: 9,
                        }}
                      >
                        <Typography
                          style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                        >
                          Beragam
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 14,
                            fontWeight: 400,
                            margin: 0,
                            marginTop: 15,
                          }}
                        >
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Ornare nec sit morbi imperdiet convallis mattis
                          eget.
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid
                    style={{
                      display: "flex",
                      marginTop: 70,
                      justifyContent: "space-between",
                    }}
                  >
                    <Grid style={{ display: "flex", width: 360 }}>
                      <Grid>
                        <img src="/Icon/IconBannerTwo-3.svg" />
                      </Grid>
                      <Grid
                        style={{
                          alignItems: "center",
                          marginLeft: 19,
                          marginTop: 9,
                        }}
                      >
                        <Typography
                          style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                        >
                          Aman
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 14,
                            fontWeight: 400,
                            margin: 0,
                            marginTop: 15,
                          }}
                        >
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Ornare nec sit morbi imperdiet convallis mattis
                          eget.
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid style={{ display: "flex", width: 360 }}>
                      <Grid>
                        <img src="/Icon/IconBannerTwo-4.svg" />
                      </Grid>
                      <Grid
                        style={{
                          alignItems: "center",
                          marginLeft: 19,
                          marginTop: 9,
                        }}
                      >
                        <Typography
                          style={{ fontSize: 20, fontWeight: 600, margin: 0 }}
                        >
                          Terbuka
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 14,
                            fontWeight: 400,
                            margin: 0,
                            marginTop: 15,
                          }}
                        >
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit. Ornare nec sit morbi imperdiet convallis mattis
                          eget.
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid style={{ margin: "100px 0px" }}>
                  <Image src="/Icon/Phone.svg" width={740} height={542} />
                </Grid>

                <Typography
                  style={{ fontSize: 16, fontWeight: 400, color: "#595959" }}
                >
                  Leverage agile frameworks to provide a robust synopsis for
                  high level overviews. Iterative approaches to corporate
                  strategy foster collaborative thinking to further the overall
                  value proposition. Organically grow the holistic world view of
                  disruptive innovation via workplace diversity and empowerment.
                  Bring to the table win-win survival strategies to ensure
                  proactive domination. At the end of the day, going forward, a
                  new normal that has evolved from generation X is on the runway
                  heading towards a streamlined cloud solution. User generated
                  content in real-time will have multiple touchpoints for
                  offshoring.
                </Typography>
              </Grid>
              <Grid
                style={{
                  display: "flex",
                  margin: "0px 275px",
                }}
              >
                {data.map((item) => (
                  <Grid
                    item
                    xs={4}
                    style={{
                      margin: "0px 19px",
                      textAlign: "center",
                      borderradius: 8,
                      marginTop: 25,
                      marginBottom: 54,
                    }}
                  >
                    <Image
                      src={item.img}
                      width={264}
                      height={264}
                      className={styles.TentangKamiImg}
                    />
                  </Grid>
                ))}
              </Grid>
              <Grid style={{ margin: "0px 295px 72px" }}>
                <Typography
                  style={{ fontSize: 16, fontWeight: 400, color: "#595959" }}
                >
                  At the end of the day, going forward, a new normal that has
                  evolved from generation X is on the runway heading towards a
                  streamlined cloud solution. User generated content in
                  real-time will have multiple touchpoints for offshoring.
                  Capitalize on low hanging fruit to identify a ballpark value
                  added activity to beta test.
                </Typography>
              </Grid>
            </Grid>
          )}
        </>
      )}
    </>
  );
}

export default TentangKami;
