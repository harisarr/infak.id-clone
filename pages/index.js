import React from "react";
import { Container } from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import { Spin } from "antd";
import "antd/dist/antd.css";
import dynamic from "next/dynamic";

function Home() {
  const BannerSectionOne = dynamic(
    () => import("../components/BannerSectionOne"),
    {
      loading: () => {
        return (
          <div
            style={{
              minHeight: "50vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        );
      },
    }
  );
  const BannerSectionTwo = dynamic(
    () => import("../components/BannerSectionTwo"),
    {
      loading: () => {
        return (
          <div
            style={{
              minHeight: "50vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        );
      },
    }
  );
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  // console.log("isMobile", isMobile);

  return (
    <Container
      style={{
        padding: 0,
        maxWidth: 9999,
      }}
    >
      {isMob ? (
        <div>
          {isLoading ? (
            <div
              style={{
                minHeight: "50vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                zIndex: 9999,
                background: "white",
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <div style={{ paddingTop: 52 }}>
              <div
                style={{
                  // paddingTop: "25px",
                  backgroundImage: isLoading
                    ? null
                    : "url(/Frame/HomepageResp.svg)",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
              >
                <BannerSectionOne />
              </div>
              <BannerSectionTwo />
            </div>
          )}
        </div>
      ) : (
        <div>
          {isLoading ? (
            <div
              style={{
                minHeight: "50vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                zIndex: 9999,
                background: "white",
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <div
              style={{
                paddingTop: "70px",
                backgroundImage: isLoading
                  ? null
                  : "url(/Frame/HomepageMainFrame.png)",
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat",
              }}
            >
              <BannerSectionOne />
              <BannerSectionTwo />
            </div>
          )}
        </div>
      )}
    </Container>
  );
}

export default Home;
