import React from "react";
import { useForm } from "react-hook-form";

function later(delay) {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
}

function HookForm() {
  const { register, handleSubmit, errors, formState, reset } = useForm();
  const onSubmit = async (data) => {
    console.log(data);
    await later();
    alert("Thankyou to register");
    reset();
  };
  return (
    <div style={{ marginTop: 200 }}>
      <h1>Create an Account</h1>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: 300,
          marginBottom: 30,
        }}
      >
        <label>Fullname *</label>
        <input
          type="text"
          name="fullName"
          ref={register({ required: "Fullname Required" })}
        />
        {errors.fullName && (
          <p style={{ color: "red" }}>{errors.fullName.message}</p>
        )}

        <label>Phone Number *</label>
        <input
          type="number"
          name="phoneNumb"
          ref={register({ required: "Phonenumb Required eh..." })}
        />
        {errors.phoneNumb && (
          <p style={{ color: "red" }}>{errors.phoneNumb.message}</p>
        )}
      </div>
      <button
        type="button"
        onClick={handleSubmit(onSubmit)}
        disabled={formState.isSubmitting}
      >
        Sign Up
      </button>
    </div>
  );
}

export default HookForm;
