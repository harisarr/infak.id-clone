import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import { Spin } from "antd";
import "antd/dist/antd.css";

function BeritaProgram_id() {
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  const data = [
    {
      cover:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
  ];

  return (
    <>
      {isMobile ? (
        isLoading ? (
          <div
            style={{
              minHeight: "100vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        ) : (
          <>
            {data.map((item) => (
              <Grid style={{ padding: "0px 7px", paddingTop: 53 }}>
                <img
                  src={item.cover}
                  style={{
                    width: "100%",
                    backgroundSize: "cover",
                  }}
                />
                <Typography
                  style={{
                    margin: "15px 5px",
                    marginBottom: 10,
                    fontWeight: 600,
                    fontSize: 36,
                  }}
                >
                  Judul Tulisan
                </Typography>
                <Grid
                  style={{
                    marginBottom: 46,
                    borderRadius: 8,
                    border: "1px solid #E0DEDD",
                  }}
                >
                  <Grid style={{ padding: "20px 20px", display: "flex" }}>
                    <Grid style={{ display: "flex", alignItems: "center" }}>
                      <Grid style={{ display: "flex" }}>
                        <img src="/Icon/Calendar.svg" />
                      </Grid>
                      <Typography
                        style={{
                          fontSize: 14,
                          fontWeight: 400,
                          marginLeft: 9,
                          whiteSpace: "nowrap",
                        }}
                      >
                        30 Sep 2018
                      </Typography>
                    </Grid>
                    <Grid
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginLeft: 30,
                      }}
                    >
                      <Grid style={{ display: "flex" }}>
                        <img src="/Icon/Person.svg" />
                      </Grid>
                      <Typography
                        style={{
                          fontSize: 14,
                          fontWeight: 400,
                          marginLeft: 9,
                          whiteSpace: "nowrap",
                        }}
                      >
                        Abdul Karim
                      </Typography>
                    </Grid>
                  </Grid>

                  <div
                    style={{
                      border: "1px solid #F6F4F3",
                      width: "100%",
                    }}
                  />

                  <Grid style={{ padding: "5px 20px" }}>
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        color: "#595959",
                      }}
                    >
                      Leverage agile frameworks to provide a robust synopsis for
                      high level overviews. Iterative approaches to corporate
                      strategy foster collaborative thinking to further the
                      overall value proposition. Organically grow the holistic
                      world view of disruptive innovation via workplace
                      diversity and empowerment. Bring to the table win-win
                      survival strategies to ensure proactive domination. At the
                      end of the day, going forward, a new normal that has
                      evolved from generation X is on the runway heading towards
                      a streamlined cloud solution. User generated content in
                      real-time will have multiple touchpoints for offshoring.
                    </Typography>
                    <img
                      src={item.cover}
                      style={{
                        width: "100%",
                        margin: "25px 0px",
                      }}
                    />
                    <Typography
                      style={{
                        fontSize: 13,
                        fontWeight: 400,
                        color: "#595959",
                      }}
                    >
                      At the end of the day, going forward, a new normal that
                      has evolved from generation X is on the runway heading
                      towards a streamlined cloud solution. User generated
                      content in real-time will have multiple touchpoints for
                      offshoring. Capitalize on low hanging fruit to identify a
                      ballpark value added activity to beta test.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            ))}
          </>
        )
      ) : isLoading ? (
        <div
          style={{
            minHeight: "100vh",
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            zIndex: 9999,
          }}
        >
          <Spin size="large" />
        </div>
      ) : (
        <>
          {data.map((item) => (
            <Grid style={{ margin: "0px 65px", marginTop: 75 }}>
              <img
                src={item.cover}
                style={{
                  width: "100%",
                  height: 550,
                  backgroundSize: "cover",
                }}
              />
              <Typography
                style={{
                  margin: "28px 0px",
                  fontWeight: 600,
                  fontSize: 36,
                }}
              >
                Judul Tulisan
              </Typography>
              <Grid
                style={{
                  marginBottom: 46,
                  borderRadius: 8,
                  border: "1px solid #E0DEDD",
                }}
              >
                <Grid style={{ padding: "29px 32px", display: "flex" }}>
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <Grid style={{ display: "flex" }}>
                      <img src="/Icon/Calendar.svg" />
                    </Grid>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontWeight: 400,
                        marginLeft: 9,
                        whiteSpace: "nowrap",
                      }}
                    >
                      30 Sep 2018
                    </Typography>
                  </Grid>
                  <Grid
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginLeft: 30,
                    }}
                  >
                    <Grid style={{ display: "flex" }}>
                      <img src="/Icon/Person.svg" />
                    </Grid>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontWeight: 400,
                        marginLeft: 9,
                        whiteSpace: "nowrap",
                      }}
                    >
                      Abdul Karim
                    </Typography>
                  </Grid>
                </Grid>

                <div
                  style={{
                    border: "1px solid #F6F4F3",
                    width: "100%",
                    marginBottom: 5,
                  }}
                />

                <Grid style={{ padding: "0px 46px", paddingBottom: 32 }}>
                  <Typography
                    style={{
                      fontSize: 16,
                      fontWeight: 400,
                      color: "#595959",
                    }}
                  >
                    Leverage agile frameworks to provide a robust synopsis for
                    high level overviews. Iterative approaches to corporate
                    strategy foster collaborative thinking to further the
                    overall value proposition. Organically grow the holistic
                    world view of disruptive innovation via workplace diversity
                    and empowerment. Bring to the table win-win survival
                    strategies to ensure proactive domination. At the end of the
                    day, going forward, a new normal that has evolved from
                    generation X is on the runway heading towards a streamlined
                    cloud solution. User generated content in real-time will
                    have multiple touchpoints for offshoring.
                  </Typography>
                  <img
                    src={item.cover}
                    style={{
                      width: "100%",
                      height: 295,
                      margin: "25px 0px",
                    }}
                  />
                  <Typography
                    style={{
                      fontSize: 16,
                      fontWeight: 400,
                      color: "#595959",
                    }}
                  >
                    At the end of the day, going forward, a new normal that has
                    evolved from generation X is on the runway heading towards a
                    streamlined cloud solution. User generated content in
                    real-time will have multiple touchpoints for offshoring.
                    Capitalize on low hanging fruit to identify a ballpark value
                    added activity to beta test.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          ))}
        </>
      )}
    </>
  );
}

export default BeritaProgram_id;
