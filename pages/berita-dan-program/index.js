import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import { Spin } from "antd";
import "antd/dist/antd.css";

function BeritaProgram() {
  const router = useRouter();
  const [isMobile, setIsMobile] = React.useState(false);
  const isMob = useMediaQuery({ maxWidth: 1024 });
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    });
  }, []);
  React.useEffect(() => {
    if (isMob) {
      setIsMobile(isMob);
    } else {
      setIsMobile(false);
    }
  }, [isMob]);
  const data = [
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
    {
      img:
        "https://www.wallpapertip.com/wmimgs/111-1119206_mecca-hd-wallpaper-masjidil-nabawi-wallpaper-iphone.jpg",
      name: "Abdul Karim",
      desc: "Promotional Advertising Specialty You Ve Waited Long Enough",
    },
  ];

  return (
    <>
      {isMob ? (
        isLoading ? (
          <div
            style={{
              minHeight: "100vh",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              zIndex: 9999,
            }}
          >
            <Spin size="large" />
          </div>
        ) : (
          <Grid style={{ padding: "53px 0px" }}>
            <Typography
              style={{
                fontSize: 25,
                fontWeight: 600,
                marginBottom: 20,
                color: "#2DBE78",
                textAlign: "center",
                textDecorationLine: "underline",
              }}
            >
              Berita dan Program
            </Typography>
            <Grid>
              {data.map((item) => (
                <Grid style={{ margin: "0px 11px", marginBottom: 24 }}>
                  <Card
                    onClick={() => router.push("/berita-dan-program/11")}
                    style={{ borderRadius: 8 }}
                  >
                    <CardActionArea>
                      <CardMedia
                        component="img"
                        alt="Mecca-FHD"
                        // height="140"
                        image={item.img}
                        title="Beautiful Mecca"
                      />
                      <CardContent>
                        <Grid style={{ display: "flex" }}>
                          <Grid
                            style={{
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <Grid style={{ display: "flex" }}>
                              <img src="/Icon/Calendar.svg" />
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                marginLeft: 9,
                                whiteSpace: "nowrap",
                              }}
                            >
                              30 Sep 2018
                            </Typography>
                          </Grid>
                          <Grid
                            style={{
                              display: "flex",
                              alignItems: "center",
                              marginLeft: 30,
                            }}
                          >
                            <Grid style={{ display: "flex" }}>
                              <img src="/Icon/Person.svg" />
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 14,
                                fontWeight: 400,
                                marginLeft: 9,
                                whiteSpace: "nowrap",
                              }}
                            >
                              Abdul Karim
                            </Typography>
                          </Grid>
                        </Grid>
                        <Typography
                          style={{
                            fontSize: 22,
                            fontWeight: 600,
                            color: "#1A1A1A",
                          }}
                        >
                          Promotional Advertising Specialty You Ve Waited Long
                          Enough
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Grid>
        )
      ) : (
        <>
          {isLoading ? (
            <div
              style={{
                minHeight: "100vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                zIndex: 9999,
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <Grid style={{ paddingTop: 72 }}>
              <Grid
                style={{ margin: "0px 99px", marginTop: 55, marginBottom: 124 }}
              >
                <Typography
                  style={{
                    fontSize: 28,
                    fontWeight: 600,
                    marginBottom: 35,
                    color: "#2DBE78",
                    textAlign: "center",
                    textDecorationLine: "underline",
                  }}
                >
                  Berita dan Program
                </Typography>
                <Grid style={{ display: "flex" }}>
                  {data.map((item) => (
                    <Grid style={{ margin: "0px 11px", marginBottom: 32 }}>
                      <Card
                        onClick={() => router.push("/berita-dan-program/11")}
                        style={{ borderRadius: 8 }}
                      >
                        <CardActionArea>
                          <CardMedia
                            component="img"
                            alt="Mecca-FHD"
                            // height="140"
                            image={item.img}
                            title="Beautiful Mecca"
                          />
                          <CardContent>
                            <Grid style={{ display: "flex" }}>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Calendar.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  30 Sep 2018
                                </Typography>
                              </Grid>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  marginLeft: 30,
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Person.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Abdul Karim
                                </Typography>
                              </Grid>
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 600,
                                color: "#1A1A1A",
                              }}
                            >
                              Promotional Advertising Specialty You Ve Waited
                              Long Enough
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
                <Grid style={{ display: "flex" }}>
                  {data.map((item) => (
                    <Grid style={{ margin: "0px 11px", marginBottom: 32 }}>
                      <Card
                        onClick={() => router.push("/berita-dan-program/12")}
                        style={{ borderRadius: 8 }}
                      >
                        <CardActionArea>
                          <CardMedia
                            component="img"
                            alt="Mecca-FHD"
                            // height="140"
                            image={item.img}
                            title="Beautiful Mecca"
                          />
                          <CardContent>
                            <Grid style={{ display: "flex" }}>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Calendar.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  30 Sep 2018
                                </Typography>
                              </Grid>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  marginLeft: 30,
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Person.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Abdul Karim
                                </Typography>
                              </Grid>
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 600,
                                color: "#1A1A1A",
                              }}
                            >
                              Promotional Advertising Specialty You Ve Waited
                              Long Enough
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
                <Grid style={{ display: "flex" }}>
                  {data.map((item) => (
                    <Grid style={{ margin: "0px 11px", marginBottom: 32 }}>
                      <Card
                        onClick={() => router.push("/berita-dan-program/13")}
                        style={{ borderRadius: 8 }}
                      >
                        <CardActionArea>
                          <CardMedia
                            component="img"
                            alt="Mecca-FHD"
                            // height="140"
                            image={item.img}
                            title="Beautiful Mecca"
                          />
                          <CardContent>
                            <Grid style={{ display: "flex" }}>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Calendar.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  30 Sep 2018
                                </Typography>
                              </Grid>
                              <Grid
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  marginLeft: 30,
                                }}
                              >
                                <Grid style={{ display: "flex" }}>
                                  <img src="/Icon/Person.svg" />
                                </Grid>
                                <Typography
                                  style={{
                                    fontSize: 14,
                                    fontWeight: 400,
                                    marginLeft: 9,
                                    whiteSpace: "nowrap",
                                  }}
                                >
                                  Abdul Karim
                                </Typography>
                              </Grid>
                            </Grid>
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 600,
                                color: "#1A1A1A",
                              }}
                            >
                              Promotional Advertising Specialty You Ve Waited
                              Long Enough
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          )}
        </>
      )}
    </>
  );
}

export default BeritaProgram;
