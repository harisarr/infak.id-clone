import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import ArrowForwardIosRoundedIcon from "@material-ui/icons/ArrowForwardIosRounded";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <ArrowForwardIosRoundedIcon
      className={className}
      style={{ ...style, display: "block", color: "#CCCCCC" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <ArrowBackIosRoundedIcon
      className={className}
      style={{ ...style, display: "block", color: "#CCCCCC" }}
      onClick={onClick}
    />
  );
}

export default function RadioButtons() {
  const settings = {
    dots: false,
    infinite: true,
    className: "center",
    autoplay: true,
    centerMode: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
          autoplay: true,
        },
      },
    ],
  };

  return (
    <div style={{ paddingTop: 100 }}>
      <div style={{ margin: "50px 72px" }}>
        <Slider {...settings}>
          <div>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <img
              src="/Frame/Partner.png"
              style={{ margin: "0 auto", borderRadius: 100 }}
            />
          </div>
        </Slider>
      </div>
    </div>
  );
}
