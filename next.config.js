module.exports = {
  env: {
    BASE_URL: process.env.BASE_URL,
    X_TENANT_ID: process.env.X_TENANT_ID,
  },
};
