import axios from "axios";

export const axiosInstance = axios.create({
  baseURL: process.env.BASE_URL,
  headers: {
    "x-tenant-id": process.env.X_TENANT_ID,
    "x-warehouse-id": null,
  },
});
