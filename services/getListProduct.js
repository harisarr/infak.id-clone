import { axiosInstance } from "./Instance";

export const getListProducts = async () => {
  const endPoint = "/customer/ecommerce/products";
  const { data } = await axiosInstance.get(endPoint);
  return data;
};
